﻿#include <iostream>

void PrintNumbers(bool bIosdd, int N)
{
	int a = 0;
	if (bIosdd)
	{
		for (a; a < N; ++a)
		{
			if (a % 2 != 0)
			{
				std::cout << a << '\n';
			}
		}
	}
	else
	{
		for (a; a < N; ++a)
		{
			if (a % 2 == 0)
			{
				std::cout << a << '\n';
			}
		}
	}

}

int main()
{
	PrintNumbers(true, 10);
	PrintNumbers(false, 20);

}